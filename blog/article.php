<!DOCTYPE html>
<html>
    <head>
        <title>Dongguo Blog</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib\bootstrap\dist\css\bootstrap.min.css">
        <link rel="stylesheet" href="css\styles.css">
    </head>
    <body>
        <script type="text/javascript" src="lib\jquery\dist\jquery.min.js"></script>
        <script type="text/javascript" src="js\javaScripts.js"></script>

        <div id="centeredContent">
            
            <?php
            if (empty($_SESSION)) {
                session_start();
            }
            require_once 'db.php';

            if (isset($_SESSION['currentUser'])) {
                $currentUsername = $_SESSION['currentUser']['username'];
                echo '<nav id="topNav">
                        <p id="welcomeStr">Your are logged in as', $currentUsername, '
                            <a href=articleadd.php>Add Article</a>                       
                            <a href=logout.php>Logout</a>               
                            <a href=welcome.php>Back Home</a></p>
                    </nav>';
                    //-------------------article--------------------------------------
                    if(isset($_GET['articleId'])){
                        $articleId = $_GET['articleId'];                        
                        $_SESSION['currentArticleId'] = $articleId;
                    }else{
                        $articleId = $_SESSION['currentArticleId'];
                    }

                        $query = "SELECT a.id, a.authorId, a.creationTime, a.title, a.body, u.username authorName " .
                            " FROM articles as a JOIN users as u ON a.authorId = u.id WHERE a.id = $articleId";
                    echo  $query; // for debugging

                    $result = mysqli_query($link, $query);
                    if (!$result) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }

                    echo "<div id=articlesList>\n";
                    while ($row = mysqli_fetch_assoc($result)) {
                        $id = $row['id'];
                        $authorId = $row['authorId'];
                        $creationTime = $row['creationTime'];
                        $title = $row['title'];
                        $body = $row['body'];
                        $authorName = $row['authorName'];
                        // print_r($row); echo "<br>\n";
                        printf("<div><a href=article.php?id=%s><b>%s</b></a><br>\nPosted by <a href=articlesByauthor.php?authorId=%s><strong>%s</strong></a> on %s<br>\n%s",
                            $id, 
                            $title, 
                            $authorId,
                            $authorName, 
                            $creationTime, 
                            $body);
                    }
                    echo "</div>\n";
                    //-------------------articles--------------------------------------

            // No account--------------------------------------------------------------
            } 
            else {
                echo '            
                    <nav id="topNav">
                        <p id="welcomeStr">Welcome to Dongguo Blog</p>
                        <p id="welcomeButton"></p>
                    </nav>
                ';
                // require_once 'login.php';
            }
            ?>
        </div>
        <div id=mycomment>
            <form method="POST">
                <h3>My comment</h3>
                <input type="submit" value="Add comment">
                <textarea name="body" cols="60" rows="5"></textarea>
                <input type="hidden" name="articleId" value="<?php echo $_SESSION['currentArticleId'] ?>" />              
                <input type="hidden" name="currentUserId" value="<?php echo $_SESSION['currentUser']['id'] ?>" />
            </form>
        </div>
        <div>

 <?php
            if (isset($_POST['currentUserId'])) { // State 2 or 3 - receiving submission
                $articleId = $_POST['articleId'];
                $authorId = $_POST['currentUserId'];
                $body = $_POST['body'];
                $errorList = array();

                if (strlen($body) < 10) {
                    array_push($errorList, "Article title must be at 10-200 characters long.");
                }

                if ($errorList) { // state 3: errors
                echo "<h3>Problems detected</h3>";
                echo "<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li>" . $error . "</li>\n";
                }
                echo "</ul>\n";
                // -----------------------------submission successful----------------------------------------
                } else { // state 2: submission successful
                    // TODO: insert record into users table  
                    if(isset($_GET['articleId'])){
                        $articleId = $_GET['articleId'];   
                    }

                    $query = sprintf("INSERT INTO comments VALUES(NULL, '%s', '%s', NULL, '%s')",
                            mysqli_real_escape_string($link, $articleId),
                            mysqli_real_escape_string($link, $authorId),
                            mysqli_real_escape_string($link, $body));
                    $result = mysqli_query($link, $query);
                    if(!$result){
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }
                    // echo "<p><a href=>submission successful</a></p>\n";
                    // echo "<p>Article added <a href=welcome.php>Go to index now</a>.</p>\n";

                    header('Location: article.php');  //redirection
                    exit;

                }
            }
        ?>

        </div>
        <div>
            <?PHP
            if (isset($_SESSION['currentArticleId'])) {
                $articleId = $_SESSION['currentArticleId'];

                $query = "SELECT c.id commentId, c.creationTime, c.body, u.username authorName " .
                        " FROM comments as c JOIN users as u ON c.authorId = u.id WHERE c.articleId = $articleId";

                echo  $query; // for debugging

                $result = mysqli_query($link, $query);
                if (!$result) {
                    echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                    exit;
                }

                echo "<div id=commentsList>\n";
                while ($row = mysqli_fetch_assoc($result)) {
                    $creationTime = $row['creationTime'];
                    $body = $row['body'];
                    $authorName = $row['authorName'];
                    // print_r($row); echo "<br>\n";
                    printf("<div><b> %s Said on %s</b><br /> %s",
                        $authorName, 
                        $creationTime, 
                        $body);
                }
                echo "</div>\n";


            }
            ?>
        </div>
       
    </body>
</html>