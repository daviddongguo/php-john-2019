<?php

if (empty($_SESSION)) {
    session_start();
    }
require_once 'db.php';

function getFrom($title = "", $body = "") {

$form = <<< MARKER
<form method="post">
    Title: <input type="text" name="title">$title<br>
           <input type="textarea" name="body">$body<br>
    <input type="submit" value="Publish Article">
</form> 
MARKER;

    return $form;
}


// 
if (!isset($_SESSION['currentUser'])) {
    echo "<p>Unauthorized, <a href=welcome.php>login first</a>.</p>";
    exit;
}



if (isset($_POST['title'])) { // State 2 or 3 - receiving submission
    $title = $_POST['title'];
    $body = $_POST['body'];
    $errorList = array();

    //  -----------------------------------Validate----------------------------------------------
    // Title
    if (strlen($title) < 2 || strlen($title) > 200) {
        array_push($errorList, "Article title must be at 2-200 characters long.");
    }

    // Content
    if (strlen($body) < 2 || strlen($body) > 10000) {
        array_push($errorList, "Article body must be at 2-10000 characters long.");
    }
    //  -----------------------------------Validate----------------------------------------------
    // ------------------------------------Errors------------------------------------------------
    if ($errorList) { // state 3: errors
        echo "<h3>Problems detected</h3>";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>" . $error . "</li>\n";
        }
        echo "</ul>\n";
        echo getForm($title, $body);
    // ------------------------------------Errors------------------------------------------------
    // -----------------------------submission successful----------------------------------------
    } else { // state 2: submission successful
        // TODO: insert record into users table  
        $authorId = $_SESSION['currentUser']['id'];
        $query = sprintf("INSERT INTO articles VALUES(NULL, '%s', NULL, '%s', '%s' )",
                mysqli_real_escape_string($link, $authorId),
                mysqli_real_escape_string($link, $title),
                mysqli_real_escape_string($link, $body));
        $result = mysqli_query($link, $query);
        if(!$result){
            echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
            exit;
        }
        // echo "<p><a href=>submission successful</a></p>\n";
        // echo "<p>Article added <a href=welcome.php>Go to index now</a>.</p>\n";
        header('Location: welcome.php');  //redirection
        exit;

    }
    // -----------------------------submission successful----------------------------------------
} else { // state 1: first show
    echo getFrom();
}

