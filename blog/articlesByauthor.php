<!DOCTYPE html>
<html>
    <head>
        <title>Dongguo Blog</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib\bootstrap\dist\css\bootstrap.min.css">
        <link rel="stylesheet" href="css\styles.css">
    </head>
    <body>
        <script type="text/javascript" src="lib\jquery\dist\jquery.min.js"></script>
        <script type="text/javascript" src="js\javaScripts.js"></script>

        <div id="centeredContent">
            
            <?php
            if (empty($_SESSION)) {
                session_start();
            }
            require_once 'db.php';

            if (isset($_SESSION['currentUser'])) {
                $currentUsername = $_SESSION['currentUser']['username'];
                echo '<nav id="topNav">
                        <p id="welcomeStr">Your are logged in as', $currentUsername, '
                            <a href=articleadd.php>Add Article</a>                       
                            <a href=logout.php>Logout</a>          
                            <a href=welcome.php>Back Home</a></p>
                    </nav>';
                    //-------------------articles--------------------------------------
                    if(isset($_GET['authorId'])){
                        $authorId = $_GET['authorId'];
                        $query = "SELECT a.id, a.authorId, a.creationTime, a.title, a.body, u.username authorName " .
                            " FROM articles as a JOIN users as u ON a.authorId = u.id WHERE u.id = $authorId";
                    }
                    echo  $query; // for debugging

                    $result = mysqli_query($link, $query);
                    if (!$result) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }

                    echo "<div id=articlesList>\n";
                    while ($row = mysqli_fetch_assoc($result)) {
                        $id = $row['id'];
                        $authorId = $row['authorId'];
                        $creationTime = $row['creationTime'];
                        $title = $row['title'];
                        $body = $row['body'];
                        $authorName = $row['authorName'];
                        // print_r($row); echo "<br>\n";
                        printf("<div><hr /><a href=article.php?id=%s><b>%s</b></a><br>\nPosted by <a href=articlesByauthor.php?authorId=%s><strong>%s</strong></a> on %s<br>\n%s",
                            $id, 
                            $title, 
                            $authorId,
                            $authorName, 
                            $creationTime, 
                            $body);
                    }
                    echo "</div>\n";

                    //-------------------articles--------------------------------------
            } else {
                echo '            
                    <nav id="topNav">
                        <p id="welcomeStr">Welcome to Dongguo Blog</p>
                        <p id="welcomeButton"></p>
                    </nav>
                ';
                require_once 'login.php';
            }
            ?>


        </div>
    </body>
</html>
