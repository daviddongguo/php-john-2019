
<?php
if (empty($_SESSION)) {
    session_start();
    }
require_once 'db.php';

if(isset($_GET['authorId'])){
    $authorId = $_GET['authorId'];
    $query = "SELECT a.id, a.authorId, a.creationTime, a.title, a.body, u.username authorName " .
        " FROM articles as a JOIN users as u ON a.authorId = u.id WHERE u.id = $authorId";
}else{
    $query = "SELECT a.id, a.authorId, a.creationTime, a.title, a.body, u.username authorName " .
        " FROM articles as a, users as u WHERE a.authorId = u.id";
}
echo  $query; // for debugging

$result = mysqli_query($link, $query);
if (!$result) {
    echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
    exit;
}

echo "<div id=articlesList>\n";
while ($row = mysqli_fetch_assoc($result)) {
    $id = $row['id'];
    $authorId = $row['authorId'];
    $creationTime = $row['creationTime'];
    $title = $row['title'];
    $body = $row['body'];
    $authorName = $row['authorName'];
    // print_r($row); echo "<br>\n";
    printf("<div><hr /><a href=article.php?articleId=%s><b>%s</b></a><br>\nPosted by <a href=articlesByauthor.php?authorId=%s><strong>%s</strong></a> on %s<br />\n%s",
        $id, 
        $title,
        $authorId,
        $authorName, 
        $creationTime, 
        $body);
}
echo "</div>\n";
?>
