<?php

require_once 'db.php';


if (isset($_GET['username'])) {
    $username = $_GET['username'];
    $query = sprintf("SELECT username FROM users WHERE username = '%s'", mysqli_real_escape_string($link, $username));
    $result = mysqli_query($link, $query);
    if (!$result) {
        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
        exit;
    }
    if (mysqli_fetch_assoc($result)) {
        echo "Username already in use";
    }
}

if (isset($_GET['email'])) {
    $email = $_GET['email'];
    $query = sprintf("SELECT email FROM users WHERE email = '%s'", mysqli_real_escape_string($link, $email));
    $result = mysqli_query($link, $query);
    if (!$result) {
        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
        exit;
    }
    if (mysqli_fetch_assoc($result)) {
        echo "Email already in use";
    }
}
