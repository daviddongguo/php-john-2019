$(document).ready(function () {
    $("input[name=username]").keyup(function () {
        var username = $("input[name=username]").val();
        $("#usernameTaken").load("istaken.php?username=" + username);
    });
});

$(document).ready(function () {
    $("input[name=email]").keyup(function () {
        var email = $("input[name=email]").val();
        $("#emailTaken").load("istaken.php?email=" + email);
    });
});