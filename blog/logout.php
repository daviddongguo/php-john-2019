<?php

session_start();
if(isset($_SESSION['currentUser'])){
    unset($_SESSION['currentUser']);    
}

// session_destroy(); // delete all session variables
// avoid using session destroy(); it reomves All session variables

header('Location: welcome.php');
?>