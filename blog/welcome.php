<!DOCTYPE html>
<html>
    <head>
        <title>Dongguo Blog</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css\styles.css">
    </head>
    <body>

        <div id="centeredContent">



            <?php

            require_once 'db.php';

            if (isset($_SESSION['currentUser'])) {
                $currentUsername = $_SESSION['currentUser']['username'];
                echo '<nav id="topNav">
                        <p id="welcomeStr">Your are logged in as', $currentUsername, '
                            <a href=articleadd.php>Add Article</a>                       
                            <a href=logout.php>Logout</a></p>
                    </nav>';
                // include_once 'index.php';
                //
                if(isset($_GET['authorId'])){
                    $authorId = $_GET['authorId'];
                    echo $authorId;
                    require_once 'index.php?authorId=1';
                }else{
                     include_once 'index.php';
                }
            } else {
                date_default_timezone_set('America/Toronto');
                $currentTime = date("Y-M-d H:i:s", time());
                echo '            
                    <nav id="topNav">
                        <p id="welcomeStr">Welcome to Dongguo Blog</p>
                        <p id="welcomeButton">', $currentTime, '  ', date_default_timezone_get(), '</p>
                    </nav>
                ';
                require_once 'login.php';
            }
            ?>


        </div>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="js\javaScripts.js"></script>
    </body>
</html>
