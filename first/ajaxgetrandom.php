<?php

if (isset($_GET['min']) && isset($_GET['max'])) {
    $min = $_GET['min'];
    $max = $_GET['max'];
    $errorList = array();

    // Validate
    if (!is_numeric($min) || !is_numeric($max)) {
        array_push($errorList, "both max($max) and min($min )must be numbers.");
    } else {
        if ($min >= $max) {
            array_push($errorList, "max($max) must be larger than the min($min).");
        }
    }

    if ($errorList) {                                               // state 2
        foreach ($errorList as $error) {
            echo $error, "<br />\n";
        }
    } else {                                                       // state 3
        echo random_int($min, $max);
    }
} else {
    echo 'Parameters are empty.';
    exit;
}
?>

