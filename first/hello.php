<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hello World</title>
    </head>
    <body>
        <?php
        
        function displayForm(){
                    ?><form >
            Name: <input name="name"><br>
            Age: <input name="age"><br>
            <input type="submit" value="Say hello">
        </form><?php
        }
        
        
        echo "<h1>David's PHP web site</h1>";
        

        // am I receiving a submission
        if(isset($_GET['name'])){
            $name = $_GET['name'];
            $age = $_GET['age'];
            if(strlen($name) < 3) {                
                echo "<p>Error: name must be at least 2 characters long.</p>";
            }
            else if($age <1 || $age > 150){
                echo "<p>Error: age must be between 1 and 150</p>";
            }
            // STATE 2: successful submission
               echo "<p>Hello $name, you $age y/o!</p>\n";
                
            displayForm();
          
            
                
            
        }else{
            // STATE 1: first show of the form
            echo "<p>Missing parameters, ";            
            echo "<strong>Please login</strong></p>";
            displayForm();
        }
                
        ?>
        
<!--        <form >
            Name: <input name="name"><br>
            Age: <input name="age"><br>
            <input type="submit" value="Say hello">
        </form>-->
    </body>
</html>
