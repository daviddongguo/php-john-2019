<?php
echo '<link rel="stylesheet" type="text/css" href="../lib/bootstrap/dist/css/bootstrap.css">';

require_once 'db.php';
// require_once 'hostinger-db.php';

                $query = sprintf("SELECT * FROM people");
                
                $result = mysqli_query($link, $query);
                
                IF(!$result) {
                    echo '<p>Error: </p>' . mysqli_errno($link);
                    exit;
                }
                echo'<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Age</th>
    </tr>
  </thead><tbody>';


                while($row = mysqli_fetch_assoc($result)){
                    $id = $row['id'];
                    $name = $row['name'];
                    $age = $row['age'];
                    echo "<tr><th scope=row>$id</th><td>$name</td><td>$age</td></tr>\n";
                    
                }
                echo '</tbody></table>';
                mysqli_close($link);
                ?>

<!-- // SQL injection attack
// User writes this in the username field of a login form
john"; DROP DATABASE user_table;

// The final query becomes this
"SELECT * FROM user_table WHERE username = john"; DROP DATABASE user_table; -->

<!-- Prepared statements do not allow the “ and ; characters to end the original query and the malicious instruction DROP DATABASE will never be executed. -->