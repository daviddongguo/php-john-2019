<?php

//session_start();
require_once 'vendor/autoload.php';

// ------------------------------ Monolog ------------------------------ 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

// ------------------------------ MeekroDB ------------------------------ 
//DB::debugMode();

DB::$host = 'localhost';
DB::$user = 'friends';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'friends';
DB::$port = 3333;
DB::$encoding = 'utf8';

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode("500 - Internal Server Error : " .
            "The server encountered an unexpected condition which prevented it from fulfilling the request." .
            "Such as invalid SQL query.");
    die; // don't want to keep going if a query broke
}

// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim();

$app->response()->header('content-type', 'application/json');
\Slim\Route::setDefaultConditions(array('id' => '\d+'));


// ----------------------------- get / ---------------------- 
$app->get('/', function() use($app, $log) {

    // Authorized by Apikey
    if (isAuthorized($_GET['apikey']) !== TRUE) {  // Unauthorized
        echo isAuthorized($_GET['apikey']);
        return;
    }

    echo 'Welcome!~';
});


// ----------------------------- get /friends ---------------------- 
$app->get('/friends', function() use($app, $log) {
    // Authorized by Apikey
    if (isAuthorized($_GET['apikey']) !== TRUE) {  // Unauthorized
        echo isAuthorized($_GET['apikey']);
        return;
    }

    $friendsList = DB::query("SELECT * FROM friends");
    echo json_encode($friendsList, JSON_PRETTY_PRINT);
});

// ----------------------------- get /friends/id ---------------------- 
$app->get('/friends/:id', function($id) use($app, $log) {
    // Authorized by Apikey
    if (isAuthorized($_GET['apikey']) !== TRUE) {  // Unauthorized
        echo isAuthorized($_GET['apikey']);
        return;
    }

    $friend = DB::queryFirstRow("SELECT * FROM friends WHERE id=%i", $id);
    echo json_encode($friend, JSON_PRETTY_PRINT);
});

// ----------------------------- post /friends ---------------------- 
$app->post('/friends', function() use($app, $log) {
    // Authorized by Apikey
    if (isAuthorized($_GET['apikey']) !== TRUE) {  // Unauthorized
        echo isAuthorized($_GET['apikey']);
        return;
    }

    $json = $app->request()->getBody();
    $friend = json_decode($json, true); // true to force it to return associative array

    if (isFriendValid($friend) !== TRUE) {      // Invalid
        $app->response()->status(400);
        $log->err("POST /friends failed: " . isFriendValid($friend));
        echo json_encode(isFriendValid($friend));
        return;
    } else {                                    // Valid
        DB::insert('friends', $friend);
        $app->response()->status(201);
        $id = DB::insertId();
        $log->debug(sprintf("POST /friend/%s succeed", $id));
        echo json_encode($id);
    }
});

// ----------------------------- put /friends/id ---------------------- 
$app->put('/friends/:id', function($id) use($app, $log) {
    $friend = json_decode($app->request()->getBody(), true); // force the body to return associative array

    if (isFriendValid($friend) !== TRUE) {      // Invalid
        $app->response()->status(400);  // Bad Request
        $log->err("PUT /friends/" . $id . "failed: " . isFriendValid($friend));
        echo json_encode(isFriendValid($friend));
        return;
    }

    $exiting = DB::queryOneRow("SELECT * FROM friends WHERE id=%i", $id);
    if (!$exiting) {
        $app->response()->status(404);  // Not Found
        $log->err("PUT /friends/" . $id . " failed: " . "%id does not exist.");
        echo json_encode("PUT /friends/" . $id . " failed: " . "%id does not exist.");
    } else {
        DB::update('friends', $friend, 'id=%i', $id);
        $log->debug(sprintf("PUT /friend/%s succeed", $id));
        echo json_encode("PUT /friend/" . $id . "  succeed");
    }
});


// ----------------------------- delete /friends/id ---------------------- 
$app->delete('/friends/:id', function($id) use($app, $log) {
    DB::delete('friends', 'id=%i', $id);
    if (DB::affectedRows() != 0) {  // success
        $log->debug(sprintf("DELETE /friends/%s succeed", $id));
        echo json_encode("DELETE /friends/" . $id . " succeed");
    } else {
        $app->response()->status(404);  // Not Found
        $log->err(sprintf("DELETE /friends/%s failed", $id));
        echo json_encode("DELETE /friends/" . $id . " failed: " . $id . " does not exist.");
    }
});

// returns TRUE if friend is valid, otherwise a string describing the problem
function isFriendValid($friend) {
    if (is_null($friend)) {
        return "JSON parsing failed.";
    }
    if (count($friend) != 2 && count($friend) != 3) {
        return "Invalid number of values";
    }
    if (!isset($friend['name']) ||
            !isset($friend['age'])) {
        return "Required field missing.";
    }
    if (strlen($friend['name']) < 2 || strlen($friend['name']) > 50) {
        return "Name must be 2-50 characters long.";
    }
    if (!is_numeric($friend['age']) || $friend['age'] < 1 || $friend['age'] > 150) {
        return "Age must be a number in 1-150.";
    }

    return TRUE;
}

function isAuthorized($apikey) {

    global $app, $log;

    if (is_null($apikey)) {
        return "ApiKey parsing failed.";
    }

    $exiting = DB::queryOneRow("SELECT apikey FROM apikeys WHERE apikey=%s", $apikey);
    if (!$exiting) {
        $msg = "Unauthorized: " . $apikey . " does not exist.";
        $app->response()->status(401);
        $log->err($msg);
        return $msg;
    }

    return TRUE;
}

// ------------------------------ Run ------------------------------ 
$app->run();
