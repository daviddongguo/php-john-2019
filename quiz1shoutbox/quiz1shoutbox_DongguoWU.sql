-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jan 10, 2019 at 07:24 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz1shout`
--

-- --------------------------------------------------------

--
-- Table structure for table `shouts`
--

CREATE TABLE `shouts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `message` varchar(100) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shouts`
--

INSERT INTO `shouts` (`id`, `name`, `message`, `ts`) VALUES
(1, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:17:38'),
(2, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:17:46'),
(3, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:28:01'),
(4, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:35:47'),
(5, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:35:51'),
(6, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:35:53'),
(12, 'Jerry', 'asd', '2019-01-10 17:42:41'),
(13, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:46:46'),
(14, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:46:47'),
(15, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:46:49'),
(16, 'Jerry', 'Hi I\'m Jerry.', '2019-01-10 17:47:00'),
(17, 'Marry', 'Hi 1', '2019-01-10 17:57:13'),
(18, 'Marry', 'Hi 2', '2019-01-10 17:57:22'),
(19, 'Marry', 'Hi 3', '2019-01-10 17:58:18'),
(20, 'David', 'Hi I\'m Jerry.', '2019-01-10 18:00:43'),
(21, 'David', ' hi, nice day!', '2019-01-10 18:01:03'),
(22, 'iwen', 'Hi I\'m Jerry.', '2019-01-10 18:01:29'),
(24, 'Marry', '2020fsdafsdafsdaaaaaaaaaafsda', '2019-01-10 18:23:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shouts`
--
ALTER TABLE `shouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shouts`
--
ALTER TABLE `shouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
