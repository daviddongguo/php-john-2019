<!DOCTYPE html>
<html>
    <head>
        <title>Quiz 1 Shoutbox -- Dongguo WU</title>
        <meta charset="UTF-8">
        <style>
            .error{
                font-weight: bold;
                background-color: yellow;
                color: red;
                border: 0;
            }
        </style>
    </head>
    <body>
        <div id="submission">
            <?php echo submitForm(); ?>
        </div>
        <div id="shoutsList">
            <?php echo getShoutsList(); ?>
        </div>
        <div id="shoutsCount">
            <?php echo getCountOfShouts(); ?>
        </div>
    </body>

    <?php

    function getCountOfShouts() {
        //You have shouted _2_ times from this browser session.
        printf("<hr /><p>You have shouted <b> %s </b> times from this browser session</p>", $_SESSION['countOfShouts']);
    }

    function getShoutsList() {
        require 'db.php';

        $query = "SELECT ts, name, message"
                . " FROM shouts"
                . " ORDER BY ts DESC"
                . " Limit 10";
//        echo $query; // for debugging
        $result = mysqli_query($link, $query);
        if (!$result) {
            echo "<p class=error>Error: SQL database query error: " . mysqli_error($link) . "</p>";
            exit;
        }
        echo '<hr /><ul >';
        while ($row = mysqli_fetch_assoc($result)) {
            $creationTime = $row['ts'];
            $name = $row['name'];
            $message = $row['message'];
            // print_r($row); echo "<br>\n";
            printf("<li>On %s <a href=user.php?user=%s><b>%s</b></a> shouted: %s</li>", $creationTime, $name, $name, $message);
        }
        echo '</ul">';
        mysqli_close($link);
    }

    function submitForm() {
        require 'db.php';

        if (isset($_POST['name']) && isset($_POST['message'])) { // State 2 or 3 - receiving submission
            $name = $_POST['name'];
            $message = $_POST['message'];
            $errorList = array();

            //  -----------------------------------Validate Start-----------------------------------------
            // Name
            if (strlen(trim($name)) < 2 || strlen(trim($name)) > 20) {
                array_push($errorList, "name($name) must be 2-20 characters long.");
            } elseif (preg_match('/^[A-Za-z0-9\ _]+$/', $name) != 1) {
                array_push($errorList, "name($name) must consist only of uppercase, lowercase characters, numbers, spaces and underscores.");
            }

            // Message
            if (strlen(trim($message)) < 1 || strlen(trim($message)) > 100) {
                array_push($errorList, "message must be 1-100 characters long.");
            }
            //  -----------------------------------Validate End---------------------------------------------
            //  
            // ------------------------------------Errors Start----------------------------------------------
            if ($errorList) { // state 3: errors
                foreach ($errorList as $error) {
                    echo "<p class=error>" . $error . "</p>\n";
                }
                echo getForm($name, $message);
                // ------------------------------------Errors  End-------------------------------------------
                // -----------------------------Submission successful----------------------------------------
            } else { // state 2: submission successful
                $query = sprintf("INSERT INTO shouts VALUES(NULL, '%s', '%s', NULL )", mysqli_real_escape_string($link, $name), mysqli_real_escape_string($link, $message));
//                echo $query;
                $result = mysqli_query($link, $query);
                if (!$result) {
                    echo "<p class=error>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                    exit;
                }
                echo "<p><a href=>submission successful</a></p>\n";
//                header('Location: 0');  //reload
                $_SESSION['countOfShouts'] ++;
                $_SESSION['currentUsername'] = $name;
                exit;
            }
            // -----------------------------submission successful----------------------------------------
        } else { // state 1: first show
            if (empty($_SESSION['currentUsername'])) {
                echo getForm();
            } else {
                echo getForm($_SESSION['currentUsername']);
            }
        }

        mysqli_close($link);
    }

    function getForm($name = "", $message = "") {

        $form = <<< MARKER
<form method="POST">
    Name:    <input type="text" name="name" value="$name" ><br>
    Message: <input type="textarea" name="message" value="$message" rows="4" cols="25"><br>
    <input type="submit" value="Shout">
</form> 
MARKER;
        return $form;
    }
    ?>
</html>



