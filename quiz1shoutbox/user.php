<?php

require 'db.php';

if (isset($_GET['user'])) {
    $username = $_GET['user'];
    $query = "SELECT ts, name, message"
            . " FROM shouts"
            . " WHERE name = '$username'"
            . " ORDER BY ts DESC"
            . " Limit 10";
//    echo $query; // for debugging
    $result = mysqli_query($link, $query);
    if (!$result) {
        echo "<p class=error>Error: SQL database query error: " . mysqli_error($link) . "</p>";
        exit;
    }
    echo '<a href="shout.php">back home</a><hr />';
    echo '<ul >';
    while ($row = mysqli_fetch_assoc($result)) {
        $creationTime = $row['ts'];
        $name = $row['name'];
        $message = $row['message'];
        // print_r($row); echo "<br>\n";
        printf("<li>On %s %s shouted: %s</li>", $creationTime, $name, $message);
    }
    echo '</ul">';
} else {
    echo 'Not found shouts';
}

mysqli_close($link);
?>

