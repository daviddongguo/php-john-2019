<?php

//session_start();
require_once 'vendor/autoload.php';

// ------------------------------ Monolog ------------------------------ 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

// ------------------------------ MeekroDB ------------------------------ 
//DB::debugMode();

DB::$host = 'localhost';
DB::$user = 'quiz2auctions';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'quiz2auctions';
DB::$port = 3333;
DB::$encoding = 'utf8';

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode("500 - Internal Server Error : " .
            "The server encountered an unexpected condition which prevented it from fulfilling the request." .
            "Such as invalid SQL query.");
    die; // don't want to keep going if a query broke
}

// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim();

$app->response()->header('content-type', 'application/json');
\Slim\Route::setDefaultConditions(array('id' => '\d+'));


// ----------------------------- get / ---------------------- 
$app->get('/', function() use($app, $log) {
    echo 'Welcome!~';
});


// -------------------- GET /auctions?sortBy=itemDesc (or any other column) ---------------------- 
$app->get('/auctions', function() use($app, $log) {
    $sortBy = $app->request()->get('sortBy', 'id');
// order by argument must not be quoted and must be sanitized by hand
    $columns = DB::columnList('auctions');
    if (!in_array($sortBy, $columns)) {
        $log->err("GET /auctions failed due to invalid sortBy argument: " + $sortBy);
        $app->response()->status(400);
        echo json_encode(false);
        return;
    }

    $auctionsList = DB::query("SELECT * FROM auctions ORDER BY %l", $sortBy);
    echo json_encode($auctionsList, JSON_PRETTY_PRINT);
});

// ----------------------------- get /auctions/id ---------------------- 
$app->get('/auctions/:id', function($id) use($app, $log) {

    $exiting = DB::queryOneRow("SELECT * FROM auctions WHERE id=%i", $id);
    if (!$exiting) {
        $app->response()->status(404);  // Not Found
        $log->err("GET /auctions/" . $id . " failed: " . "%id does not exist.");
        echo json_encode("GET /auctions/" . $id . " failed: " . "%id does not exist.");
    } else {
        $log->debug(sprintf("GET /auction/%s succeed", $id));
        echo json_encode($exiting, JSON_PRETTY_PRINT);
    }
});

// ----------------------------- post /auctions ---------------------- 
$app->post('/auctions', function() use($app, $log) {

    $json = $app->request()->getBody();
    $auction = json_decode($json, true); // true to force it to return associative array
// make sure seller email is valid and itemDesc is 1-200 characters long
    if (isAuctionValidByPost($auction) !== TRUE) {      // Invalid
        $app->response()->status(400);
        $log->err("POST /auctions failed: " . isAuctionValidByPost($auction));
        echo json_encode(isAuctionValidByPost($auction));
        return;
    } else {                                    // Valid
        DB::insert('auctions', $auction);
        $app->response()->status(201);
        $id = DB::insertId();
        $log->debug(sprintf("POST /auction/%s succeed", $id));
        echo json_encode($id);
    }
});

// ----------------------------- put /auctions/id ---------------------- 
$app->put('/auctions/:id', function($id) use($app, $log) {
    $auction = json_decode($app->request()->getBody(), true); // force the body to return associative array
// //If it is not then 400 error is generated and new bid is rejected.

    $existinglastBid = DB::queryOneRow("SELECT lastBid FROM auctions WHERE id=%i", $id);
    if (!$existinglastBid) {
        $app->response()->status(404);  // Not Found
        $log->err("PUT /auctions/" . $id . " failed: " . "%id does not exist.");
        echo json_encode("PUT /auctions/" . $id . " failed: " . "%id does not exist.");
    } else {
        if (isAuctionValidByPut($auction) !== TRUE) {       // Invalid
            $app->response()->status(400);
            $log->err("PUT /auctions failed: " . isAuctionValidByPut($auction));
            echo json_encode(isAuctionValidByPut($auction));
            return;
        } else {
            if ($existinglastBid['lastBid'] >= $auction['lastBid']) {
                $app->response()->status(400);
                $msg = "PUT /auctions failed: the new lastBid("
                        .$existinglastBid['lastBid']. 
                        ") isn't higher than the existing lastBid("
                        .$auction['lastBid']. ")";
                $log->err($msg);
                echo json_encode($msg);
                return;
            } else {
                DB::update('auctions', $auction, 'id=%i', $id);
                $log->debug(sprintf("PUT /auction/%s succeed", $id));
                echo json_encode("PUT /auction/" . $id . "  succeed");
            }
        }
    }
});

function isAuctionValidByPost($auction) {
    if (is_null($auction)) {
        return "JSON parsing failed.";
    }
    if (count($auction) != 2) {
        return "Invalid number of values";
    }
    if (!isset($auction['itemDesc']) || !isset($auction['sellerEmail'])) {
        return "Required field missing";
    }


    if (strlen($auction['itemDesc']) < 1 || strlen($auction['itemDesc']) > 200) {
        return "Name must be 1-200 characters long.";
    }
    if (filter_var($auction['sellerEmail'], FILTER_VALIDATE_EMAIL) === FALSE) {
        return "Seller email is invalid.";
    }

    return TRUE;
}

function isAuctionValidByPut($auction) {
    if (is_null($auction)) {
        return "JSON parsing failed.";
    }
    if (count($auction) != 2) {
        return "Invalid number of values";
    }
    if (!isset($auction['lastBid']) || !isset($auction['lastBidderEmail'])) {
        return "Required field missing";
    }

    if (!is_numeric($auction['lastBid'])) {
        return "Last bid is not a number.";
    }
    if (filter_var($auction['lastBidderEmail'], FILTER_VALIDATE_EMAIL) === FALSE) {
        return "Last bidder email is invalid.";
    }

    return TRUE;
}

// ------------------------------ Run ------------------------------ 
$app->run();
