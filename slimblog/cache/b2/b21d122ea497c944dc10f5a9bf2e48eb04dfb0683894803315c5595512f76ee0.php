<?php

/* register.html.twig */
class __TwigTemplate_f7e257a1a833bf21677b6177ffa85ab29287b030c5e1db1c8d9cc8c9d1fb458e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Register";
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
";
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
        // line 19
        echo "
    ";
        // line 20
        if (($context["errorList"] ?? null)) {
            // line 21
            echo "        <ul class=\"error\">
            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 23
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "        </ul>
    ";
        }
        // line 27
        echo "
    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value=";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", array()), "html", null, true);
        echo "><br />
        <span id=\"emailTaken\" class=\"error\"></span><br>
        Password:  <input type=\"password\" name=\"password\" value=\"Ja1Ja1Ja1\"><br />
        Password(repeated):  <input type=\"password\" name=\"password2\" value=\"Ja1Ja1Ja1\"><br />
        <input type=\"submit\" value=\"Register\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 29,  80 => 27,  76 => 25,  67 => 23,  63 => 22,  60 => 21,  58 => 20,  55 => 19,  52 => 18,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Register{% endblock %}

{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
{% endblock addhead%}


{% block content %}

    {% if errorList %}
        <ul class=\"error\">
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value={{v.email}}><br />
        <span id=\"emailTaken\" class=\"error\"></span><br>
        Password:  <input type=\"password\" name=\"password\" value=\"Ja1Ja1Ja1\"><br />
        Password(repeated):  <input type=\"password\" name=\"password2\" value=\"Ja1Ja1Ja1\"><br />
        <input type=\"submit\" value=\"Register\">
    </form>

{% endblock content %}", "register.html.twig", "C:\\xampp\\htdocs\\php-john\\slimblog\\templates\\register.html.twig");
    }
}
