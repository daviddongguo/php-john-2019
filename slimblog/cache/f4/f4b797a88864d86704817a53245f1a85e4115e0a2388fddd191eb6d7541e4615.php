<?php

/* login.html.twig */
class __TwigTemplate_a88c3403678bcf16ead73a383a773ad10afdf11571cf29ee0917f1b8f6d58a63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'addfoot' => array($this, 'block_addfoot'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Login";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "
    ";
        // line 8
        if (($context["error"] ?? null)) {
            echo "    
        <p class=\"error\">Login credentials invalid<br>
            Try again or <a href=\"/register\">register</a></p>
        ";
        }
        // line 12
        echo "    <ul>
        <li>Default User: gu@es.t</li>
        <li>Default password: 123asD ";
        // line 14
        echo twig_escape_filter($this->env, ($context["timeStamp"] ?? null), "html", null, true);
        echo "</li>
    </ul>

    >

    Email:  <input type=\"email\" id=\"email\" value=";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", array()), "html", null, true);
        echo "><br />
    password: <input type=\"password\" id=\"password\" value=\"123asD\"><br />
    <button id=\"cancelButton\">Cancel</button>
    <button id=\"loginButton\">Login</button>


    ";
        // line 25
        $this->displayBlock('addfoot', $context, $blocks);
        // line 56
        echo "
";
    }

    // line 25
    public function block_addfoot($context, array $blocks = array())
    {
        // line 26
        echo "        <script type=\"text/javascript\" src=\"jquery.js\"></script>
        <script type=\"text/javascript\" src=\"jQuery.md5.js\"></script>
        <script type=\"text/javascript\">
            \$(\"#loginButton\").click(function () {
                var emailVal = \$('email').val();
                var password = \$('password').val();
                var complexPasswordVal = initPwd(password);
                console.log('";
        // line 33
        echo twig_escape_filter($this->env, ($context["timeStamp"] ?? null), "html", null, true);
        echo "');
                console.log(complexPassword);

                var user = ({'email': emailVal, 'password': complexPasswordVal});
                \$.ajax({
                    url: '/slimblog.php/api/login',
                    data: JSON.stringify(user),
                    type: \"POST\",
                    dataType: 'json'
                }).done(function () {
                    alert(\"addedd successfully\");
                });

            });
            function initPwd(pwd) {
                var hashcode = window.location.href.replace(\" \", \"\");
                pwd = hashcode + \$.md5(pwd) + \$.md5('";
        // line 49
        echo twig_escape_filter($this->env, ($context["timeStamp"] ?? null), "html", null, true);
        echo "');
                pwd = \$.md5(pwd);
                return pwd;
            }
        </script>

    ";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 49,  89 => 33,  80 => 26,  77 => 25,  72 => 56,  70 => 25,  61 => 19,  53 => 14,  49 => 12,  42 => 8,  39 => 7,  36 => 6,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Login{% endblock %}


{% block content %}

    {% if error %}    
        <p class=\"error\">Login credentials invalid<br>
            Try again or <a href=\"/register\">register</a></p>
        {% endif %}
    <ul>
        <li>Default User: gu@es.t</li>
        <li>Default password: 123asD {{ timeStamp }}</li>
    </ul>

    >

    Email:  <input type=\"email\" id=\"email\" value={{v.email}}><br />
    password: <input type=\"password\" id=\"password\" value=\"123asD\"><br />
    <button id=\"cancelButton\">Cancel</button>
    <button id=\"loginButton\">Login</button>


    {% block addfoot %}
        <script type=\"text/javascript\" src=\"jquery.js\"></script>
        <script type=\"text/javascript\" src=\"jQuery.md5.js\"></script>
        <script type=\"text/javascript\">
            \$(\"#loginButton\").click(function () {
                var emailVal = \$('email').val();
                var password = \$('password').val();
                var complexPasswordVal = initPwd(password);
                console.log('{{timeStamp}}');
                console.log(complexPassword);

                var user = ({'email': emailVal, 'password': complexPasswordVal});
                \$.ajax({
                    url: '/slimblog.php/api/login',
                    data: JSON.stringify(user),
                    type: \"POST\",
                    dataType: 'json'
                }).done(function () {
                    alert(\"addedd successfully\");
                });

            });
            function initPwd(pwd) {
                var hashcode = window.location.href.replace(\" \", \"\");
                pwd = hashcode + \$.md5(pwd) + \$.md5('{{timeStamp}}');
                pwd = \$.md5(pwd);
                return pwd;
            }
        </script>

    {% endblock %}

{% endblock content %}

", "login.html.twig", "C:\\xampp\\htdocs\\php-john\\slimblog\\templates\\login.html.twig");
    }
}
