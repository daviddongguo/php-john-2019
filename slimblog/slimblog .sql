-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jan 17, 2019 at 10:48 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slimblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(200) NOT NULL,
  `body` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `authorId`, `creationTime`, `title`, `body`) VALUES
(1, 1, '2019-01-09 21:39:34', 'the first article ', 'Guess what! You can now access phpMyAdmin area directly using this link. It should save you or your developers a hassle of navigating through cPanel every time you want to tweak your database!'),
(2, 1, '2019-01-09 21:41:00', 'the second article', 'Guess what! You can now access phpMyAdmin area directly using this link. It should save you or your developers a hassle of navigating through cPanel every time you want to tweak your database!'),
(3, 1, '2019-01-10 14:18:55', 'mysqli_query', 'Returns FALSE on failure. For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries mysqli_query() will return a mysqli_result object. For other successful queries mysqli_query() will return TRUE.'),
(4, 4, '2019-01-14 14:06:25', '2', '3');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `articleId` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `body` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `articleId`, `authorId`, `creationTime`, `body`) VALUES
(1, 1, 1, '2019-01-10 13:59:23', 'Very good! Very good! Very good! Very good! Very good! Very good! Very good! '),
(7, 1, 1, '2019-01-10 14:11:50', 'echo \"<script>alert(\'Cross-site Scripting (XSS) refers to client-side code injection attack\');</script>\";'),
(8, 3, 1, '2019-01-10 14:19:18', 'Note:\r\n\r\nIn the case where you pass a statement to mysqli_query() that is longer than max_allowed_packet of the server, the returned error codes are different depending on whether you are using MySQL Native Driver (mysqlnd) or MySQL Client Library (libmysqlclient). The behavior is as follows:\r\n\r\nmysqlnd on Linux returns an error code of 1153. The error message means \"got a packet bigger than max_allowed_packet bytes\".\r\n\r\nmysqlnd on Windows returns an error code 2006. This error message means \"server has gone away\".\r\n\r\nlibmysqlclient on all platforms returns an error code 2006. This error message means \"server has gone away\".');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'Jerry123@blog.com', 'Jerry123'),
(2, 'jerry@c.cn', 'Je1'),
(3, 'wudongguo126@gmail.com', 'Je3'),
(4, 'w@w.c', 'Ja1Ja1Ja1'),
(7, 'w@w.d', 'Ja1Ja1Ja1'),
(8, 'w@e.e', 'Ja1Ja1Ja1'),
(10, 'w@w.e', 'Ja1Ja1Ja1'),
(11, 'w@w.c.c', 'Ja1Ja1Ja1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorId` (`authorId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articleId` (`articleId`),
  ADD KEY `authorId` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `artricles_ibfk_1` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`articleId`) REFERENCES `articles` (`id`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
