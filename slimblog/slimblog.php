<?php

session_start();
require_once 'vendor/autoload.php';

//DB::debugMode();
// for MeekroDB
if (true) {
    DB::$host = 'localhost';
    DB::$user = 'blog';
    DB::$password = 'vuxunjqTbm5S7sAq';
    DB::$dbName = 'blog';
    DB::$port = 3333;
    DB::$encoding = 'utf8';
} else {
    DB::$host = 'localhost';
    DB::$user = 'cp4907_dongguo';
    DB::$password = 'vuxunjqTbm5S7sAq';
    DB::$dbName = 'cp4907_dongguo';
    DB::$encoding = 'utf8';
}




// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');
// ------------------- Slim creation and setup ------------------- 

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}
$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);

// -------------------------------- / -------------------------------- 
$app->get('/', function() use($app) {
    // Login firstly
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }
    // Receive subbmission
    $articleList = DB::query("SELECT a.id, a.creationTime, a.title, a.body, u.email authorName " .
                    " FROM articles as a, users as u WHERE a.authorId = u.id");
    $app->render('index.html.twig', array('al' => $articleList));
});

// -------------------------------- /article/:id -------------------------------- 
$app->post('/article/:id', function($id) use($app) {
    // Login firstly
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }

    $article = DB::queryFirstRow("SELECT a.id, a.creationTime, a.title, a.body, u.email authorName " .
                    " FROM articles as a, users as u WHERE a.authorId = u.id AND a.id=%s", $id);
    $app->render("article_view.html.twig", array('a' => $article));
});

// -------------------------------- /articleadd -------------------------------- 
$app->get('/articleadd', function() use($app) {
    // Login firstly
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }

    // Receive subbmission
    $authorId = $_SESSION['user']['id'];
    $title = $app->request()->get('title');
    $body = $app->request()->get('body');
    $valueList = array(
        'id' => $authorId,
        'title' => $title,
        'body' => $body);

    // state 1 first show    
    $app->render('article_add.html.twig', array(
        v => $valueList
    ));
});
$app->post('/articleadd', function() use ($app) {
    // login firstly
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }

    // receiving submission
    $authorId = $_SESSION['user']['id'];
    $title = $app->request()->post('title');
    $body = $app->request()->post('body');
    $valueList = array(
        'authorId' => $authorId,
        'title' => $title,
        'body' => $body
    );

    // verify submission
    $errorList = array();
    if (strlen($title) < 2 || strlen($title) > 200) {
        array_push($errorList, "Title must be between 2-200 characters long");
    }
    if (strlen($body) < 2 || strlen($body) > 10000) {
        array_push($errorList, "Article body must be between 2-10000 characters long");
    }
    //
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('articles', $valueList);
        $articleId = DB::insertId();
        $app->render('article_add_success.html.twig', array('articleId' => $articleId));
    } else {
        // state 3: failed submission
        $app->render('article_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
        ));
    }
});

// ----------------------------------------------------------------------------- 
$app->get('/isemailregistered/:email', function($email = "") {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    echo ($user) ? "Email already in use." : "";
});

// -----------------------  /register     ------------------------------------- 
$app->get('/register', function() use($app) {
    // state 1 first show    
    $app->render('register.html.twig', array(
        'v' => array('email' => $app->request()->get('email'))
    ));
});
$app->post('/register', function() use($app) {
    // receiving submission
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    $password2 = $app->request()->post('password2');
    $valueList = array('email' => $email);

    // verify submission
    $errorList = array();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        unset($valueList['email']);
        array_push($errorList, "email($email)is invalid");
    } else {
        $row = DB::queryFirstRow("SELECT email FROM users WHERE email=%s LIMIT 1", $email);
        if ($row) {
            unset($valueList['email']);
            array_push($errorList, "email($email) already be in use");
        }
    }
    if (strlen($password) < 6 || strlen($password2) < 6) {
        array_push($errorList, "Password must be at least 6 characters long");
    } else {
        if ($password != $password2) {
            array_push($errorList, "Passwords do not match.");
        }
    }

    // 
    if (!$errorList) {
        // sstate 2: successful submission
        DB::insert('users', array(
            'email' => $email,
            'password' => md5($password)
        ));

        $app->render('register_success.html.twig', array(
            'v' => $valueList
        ));
    } else {
        // state 3 Failed submission
        $app->render('register.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
        ));
    }
});

// -------------------------------- login -------------------------------- 
$app->get('/login', function() use($app) {
    // state 1 first show    
    $valueList = array('email' => $app->request()->get('email'));
    $_SESSION['TimeStamp'] = substr(md5(time()), 5, 8);
    $app->render('login.html.twig', array(
        'v' => $valueList,
        'timeStamp' => $_SESSION['TimeStamp']
    ));
});
$app->post('/login', function() use ($app) {
    // receiving submission
    $email = $app->request()->post('email');
    $password = md5($app->request()->post('password'));
    $valueList = array('email' => $email);
    // verify submission
    $isLoginSuccessful = false;

    //
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user && ($user['password'] == $password)) {
        $isLoginSuccessful = true;
    }
    //
    if ($isLoginSuccessful) {
        // state 2: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('login_success.html.twig', array(
            'v' => $valueList));
    } else {
        // state 3: failed submission
        $app->render('login.html.twig', array('error' => true));
    }
});

$app->post('/api/login', function() use ($app) {
    // receiving submission
    $json = $app->request()->getBody();
    $userClient = json_decode($json, true); // true to force it to return 


    $email = $userClient['email'];
    $password = $userClient['password'];
    $valueList = array('email' => $email);
    // verify submission
    $isLoginSuccessful = false;

    //
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user && (getPwd($user['password']) == $password)) {
        $isLoginSuccessful = true;
    }
    //
    if ($isLoginSuccessful) {
        // state 2: successful submission
//        unset($user['password']);
//        $_SESSION['user'] = $user;
//        $app->render('login_success.html.twig', array(
//            'v' => $valueList));
        echo 'success';
    } else {
        // state 3: failed submission
//        $app->render('login.html.twig', array('error' => true));
        echo 'failed';
    }
});

// -------------------------------- logout -------------------------------- 
$app->get('/logout', function() use($app) {
    $_SESSION['user'] = array();
    $app->render('index.html.twig');
});

// -------------------------------- session -------------------------------- 
$app->get('/session', function() {

    echo "<pre>SESSION: \n\n";  // for debugging
    var_dump($_SESSION);        // for debugging
});

// ----------------------------function getPwd($pwd) -------------------------------- 
function getPwd($pwd) {
    $hashcode = trim($_SERVER["HTTP_REFERER"]);
    $pwd = $hashcode . $pwd . md5($_SESSION['TimeStamp']);
    $pwd = md5($pwd);
    return $pwd;
}

$app->run();
