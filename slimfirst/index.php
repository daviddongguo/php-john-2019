<?php

require_once 'vendor/autoload.php';

//DB::debugMode();


// for MeekroDB
DB::$host = 'localhost';
DB::$user = 'first';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'first';
DB::$port = 3333;
DB::$encoding = 'utf8';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


$app->get('/hello/:name/:age', function ($name, $age) use($app) {
//    echo "Hello, $name, you are $age y/o.";


    DB::insert('people', array(
        'name' => $name,
        'age' => $age,
    ));

    $app->render('hello.html.twig', array(
        'name' => $name,
        'age' => $age,
    ));
});

$app->get('/list', function() {
    $peopleList = DB::query("SELECT * FROM people");
    print_r($peopleList);
    echo 'list:\n';
    foreach ($peopleList as $row) {
        echo "Name: " . $row['name'] . "\n";
        echo "Age: " . $row['age'] . "\n";
        echo "-------------\n";
    }
});


$app->get('/admin', function() use($app) {
    // state 1: first show
    $app->render('admin.html.twig', array(
        'name' => trim($app->request()->get('name')),
        'age' => trim($app->request()->get('age')),
    ));
});

$app->post('/admin', function() use($app) {
    // receiving submissing
    $name = trim($app->request()->post('name'));
    $age = trim($app->request()->post('age'));

    // verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 20) {
        array_push($errorList, 'Name($name) must be 2-20 characters long.');
    }
    if (!is_numeric($age) || $age < 1 || $age > 150) {
        array_push($errorList, 'Age($age) must be an integer in 1-150 rage.');
    }

    // 
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('people', array(
            'name' => $name,
            'age' => $age,
        ));
        $app->render('addme_success.html.twig');
    } else {
        // state 3: failed submission
        $app->render('admin.html.twig', array(
            'name' => $name,
            'age' => $age,
            'errorList' => $errorList
        ));
    }
});

// Random()
$app->get('/random', function() use($app) {
    // state 1: first show
    $app->render('random.html.twig', array(
        'min' => trim($app->request()->get('min')),
        'max' => trim($app->request()->get('max')),
        'count' => trim($app->request()->get('count')),
    ));
});

$app->post('/random', function() use($app) {
    // receiving submissing
    $min = trim($app->request()->post('min'));
    $max = trim($app->request()->post('max'));
    $count = trim($app->request()->post('count'));

    // verify submission
    $errorList = array();
    if (!is_numeric($min) || !is_numeric($max)) {
        array_push($errorList, 'min and max must be numerical');
    } else {
        if ($min > $max) {
            array_push($errorList, 'min must be less or equal to max.');
        }
    }
    if (!is_numeric($count) || $count < 1) {
        array_push($errorList, 'count must be numerical and 1 or greater');
    }

    // 
    if (!$errorList) {
        // state 2: successful submission
        $numbers = array();
        for ($i=0; $i < $count; $i++){
            array_push($numbers, random_int($min, $max));
        }
        $app->render('random.html.twig', array(
            'min' => $min,
            'max' => $max,
            'count' => $count,
            'numbers' => $numbers,
        )
            
            );
    } else {
        // state 3: failed submission
        $app->render('random.html.twig', array(
            'min' => $min,
            'max' => $max,
            'count' => $count,
            'errorList' => $errorList
        ));
    }
});


$app->run();
