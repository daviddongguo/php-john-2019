<?php

/* fatal_error.html.twig */
class __TwigTemplate_1dc0a480b3301fb712ab2f2e52e0c1d5c8760ecbc8e67b66c216e98e9d3fa5ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "fatal_error.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Fatal Error";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "

    <h1>failure</h1>
    <p></p>
    <img src=\"/images/ninja.jpg\" width=\"200px\">
    ";
        // line 11
        if (($context["errorList"] ?? null)) {
            echo "    
        <ul class=\"error\">
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 14
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </ul>
    ";
        }
        // line 18
        echo "
";
    }

    public function getTemplateName()
    {
        return "fatal_error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 18,  63 => 16,  54 => 14,  50 => 13,  45 => 11,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Fatal Error{% endblock %}

{% block content %}


    <h1>failure</h1>
    <p></p>
    <img src=\"/images/ninja.jpg\" width=\"200px\">
    {% if errorList %}    
        <ul class=\"error\">
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

{% endblock content%}

", "fatal_error.html.twig", "C:\\xampp\\htdocs\\ipd15-david\\slimtest\\templates\\fatal_error.html.twig");
    }
}
