<?php

/* passport_add.html.twig */
class __TwigTemplate_239f8064acc350c28d807b0b29b02c70829a5e3c6fc7211654798a5deaf0998c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "passport_add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add Passport";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    ";
        // line 7
        if (($context["errorList"] ?? null)) {
            echo "    
        <ul>
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        </ul>
    ";
        }
        // line 14
        echo "
    <form method=\"post\" enctype=\"multipart/form-data\">
        Number: <input type=\"text\" name=\"number\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "name", array()), "html", null, true);
        echo "\"><br>
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "name", array()), "html", null, true);
        echo "\"><br>
        Image: <input type=\"file\" name=\"image\" ><br>
        <input type=\"submit\" value=\"Add passport\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "passport_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 17,  67 => 16,  63 => 14,  59 => 12,  50 => 10,  46 => 9,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add Passport{% endblock %}

{% block content %}

    {% if errorList %}    
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\" enctype=\"multipart/form-data\">
        Number: <input type=\"text\" name=\"number\" value=\"{{v.name}}\"><br>
        Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
        Image: <input type=\"file\" name=\"image\" ><br>
        <input type=\"submit\" value=\"Add passport\">
    </form>

{% endblock %}", "passport_add.html.twig", "C:\\xampp\\htdocs\\ipd15-david\\slimtest\\templates\\passport_add.html.twig");
    }
}
