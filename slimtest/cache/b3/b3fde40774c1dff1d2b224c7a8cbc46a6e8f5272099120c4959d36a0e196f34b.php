<?php

/* master.html.twig */
class __TwigTemplate_4af65d0d1ace238c8af7f5a52ea00ee689db0387988a7eb2638699586e12b2c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
            'addfoot' => array($this, 'block_addfoot'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"../../styles.css\" />
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " - Todo </title>
        ";
        // line 7
        $this->displayBlock('addhead', $context, $blocks);
        // line 9
        echo "    </head>
    <body>
        <div id=\"centeredContent\">


            <div id='topNav'>
                <ul class=\"nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" href=\"/\">Lists</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link disabled\" href=\"/login\" tabindex=\"-1\" aria-disabled=\"true\">Login</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" href=\"/add\" tabindex=\"-1\" aria-disabled=\"true\">Add Todo</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link disabled\" href=\"/logout\" tabindex=\"-1\" aria-disabled=\"true\">Logout</a>
                    </li>
                </ul>
            </div>

        ";
        // line 31
        $this->displayBlock('content', $context, $blocks);
        // line 32
        echo "
        <div id=\"footer\">
            &copy; Copyright 2019 <a href=\"http://localhost:8003\">Back Home</a>.
        </div>
        <!-- Todo: Google-->
    </div>
    <!-- TODO: add Google Analytics tracking code -->
    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
    ";
        // line 42
        $this->displayBlock('addfoot', $context, $blocks);
        // line 44
        echo "</body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 7
    public function block_addhead($context, array $blocks = array())
    {
        // line 8
        echo "        ";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
    }

    // line 42
    public function block_addfoot($context, array $blocks = array())
    {
        // line 43
        echo "    ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  101 => 43,  98 => 42,  93 => 31,  89 => 8,  86 => 7,  81 => 6,  76 => 44,  74 => 42,  62 => 32,  60 => 31,  36 => 9,  34 => 7,  30 => 6,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"../../styles.css\" />
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <title>{% block title %}{% endblock %} - Todo </title>
        {% block addhead %}
        {% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">


            <div id='topNav'>
                <ul class=\"nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" href=\"/\">Lists</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link disabled\" href=\"/login\" tabindex=\"-1\" aria-disabled=\"true\">Login</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" href=\"/add\" tabindex=\"-1\" aria-disabled=\"true\">Add Todo</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link disabled\" href=\"/logout\" tabindex=\"-1\" aria-disabled=\"true\">Logout</a>
                    </li>
                </ul>
            </div>

        {% block content %}{% endblock %}

        <div id=\"footer\">
            &copy; Copyright 2019 <a href=\"http://localhost:8003\">Back Home</a>.
        </div>
        <!-- Todo: Google-->
    </div>
    <!-- TODO: add Google Analytics tracking code -->
    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
    {% block addfoot %}
    {% endblock %}
</body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd15-david\\slimtest\\templates\\master.html.twig");
    }
}
