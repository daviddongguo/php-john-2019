<?php

/* todo_add.html.twig */
class __TwigTemplate_59f98aa8ac6c4c44f62424f4635d9543d99b83bff8aa184310c6846182df31e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "todo_add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'addfoot' => array($this, 'block_addfoot'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
    ";
        // line 7
        if (($context["errorList"] ?? null)) {
            echo "    
        <ul>
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        </ul>
    ";
        }
        // line 14
        echo "
    <form id=\"taskForm\" method=\"post\">
        Task: <input type=\"text\" name=\"task\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "task", array()), "html", null, true);
        echo "\"><br />
        DueDate: <input type=\"date\" name=\"dueDate\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "dueDate", array()), "html", null, true);
        echo "\"><br />
        <input type=\"checkbox\" id=\"isDone\" name=\"isDone\" value=\"true\" checked> IsDone<br /> 
        <input type=\"hidden\"  name=\"id\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "id", array()), "html", null, true);
        echo "\"><br>
        <input type=\"submit\" value=\"Add Todo\">
    </form>

    ";
        // line 23
        if (($context["v"] ?? null)) {
            echo "    
        <ul>
            ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["v"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 26
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "        </ul>
    ";
        }
        // line 30
        echo "
";
    }

    // line 33
    public function block_addfoot($context, array $blocks = array())
    {
        // line 40
        echo "          \$(\"input[name=dueDate]\").value = todayStr;
          alert(\$(\"input[name=dueDate]\").value);

          if (true) {
              \$(\"input[name=isDone]\").checked = true;
          }

";
        // line 48
        echo "          alert((\"input[name=task]\").value);

      });
    </script>#}
";
    }

    public function getTemplateName()
    {
        return "todo_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 48,  114 => 40,  111 => 33,  106 => 30,  102 => 28,  93 => 26,  89 => 25,  84 => 23,  77 => 19,  72 => 17,  68 => 16,  64 => 14,  60 => 12,  51 => 10,  47 => 9,  42 => 7,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add{% endblock %}

{% block content %}

    {% if errorList %}    
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form id=\"taskForm\" method=\"post\">
        Task: <input type=\"text\" name=\"task\" value=\"{{v.task}}\"><br />
        DueDate: <input type=\"date\" name=\"dueDate\" value=\"{{v.dueDate}}\"><br />
        <input type=\"checkbox\" id=\"isDone\" name=\"isDone\" value=\"true\" checked> IsDone<br /> 
        <input type=\"hidden\"  name=\"id\" value=\"{{v.id}}\"><br>
        <input type=\"submit\" value=\"Add Todo\">
    </form>

    {% if v %}    
        <ul>
            {% for error in v %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

{% endblock content%}

{% block addfoot %}
{#    <script>
        \$(\"#taskForm\").ready(function () {

            var today = new Date();
            var todayStr = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
        {#          alert(todayStr);#}
          \$(\"input[name=dueDate]\").value = todayStr;
          alert(\$(\"input[name=dueDate]\").value);

          if (true) {
              \$(\"input[name=isDone]\").checked = true;
          }

{#          \$(\"input[name=task]\").value = \"buy ...\";#}
          alert((\"input[name=task]\").value);

      });
    </script>#}
{% endblock addfoot%}



", "todo_add.html.twig", "C:\\xampp\\htdocs\\ipd15-david\\slimtodo\\templates\\todo_add.html.twig");
    }
}
