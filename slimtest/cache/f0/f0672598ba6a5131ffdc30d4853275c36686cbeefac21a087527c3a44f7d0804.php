<?php

/* index.html.twig */
class __TwigTemplate_e707f1d4b34f152f7b13ce8b6b6e15ae909a099529e0f4ef32e9ad78085af5da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"artList\">
        <table class=\"table table-striped\">
            <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Task</th>
                    <th scope=\"col\">DueDate</th>
                    <th scope=\"col\">IsDone</th>
                    <th scope=\"col\"></th>
                    <th scope=\"col\"></th>
                </tr>
            </thead>
            <tbody>
                ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["todosList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 20
            echo "                    <tr>
                        <th scope=\"row\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "</th>
                        <td><a href=\"/edit/";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "task", array()), "html", null, true);
            echo "</a></td>
                        <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "dueDate", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "isDone", array()), "html", null, true);
            echo "</td>
                        <td><a href=\"/edit/";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Edit</a></td>
                        <td><a href=\"/delete/";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Delete</a></td>
                    </tr>  
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </tbody>
        </table>
    </div>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  82 => 26,  78 => 25,  74 => 24,  70 => 23,  64 => 22,  60 => 21,  57 => 20,  53 => 19,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Index{% endblock %}

{% block content %}
    <div class=\"artList\">
        <table class=\"table table-striped\">
            <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Task</th>
                    <th scope=\"col\">DueDate</th>
                    <th scope=\"col\">IsDone</th>
                    <th scope=\"col\"></th>
                    <th scope=\"col\"></th>
                </tr>
            </thead>
            <tbody>
                {% for t in todosList %}
                    <tr>
                        <th scope=\"row\">{{t.id}}</th>
                        <td><a href=\"/edit/{{t.id}}\">{{t.task}}</a></td>
                        <td>{{t.dueDate}}</td>
                        <td>{{t.isDone}}</td>
                        <td><a href=\"/edit/{{t.id}}\">Edit</a></td>
                        <td><a href=\"/delete/{{t.id}}\">Delete</a></td>
                    </tr>  
                {% endfor %}
            </tbody>
        </table>
    </div>
{% endblock content%}

", "index.html.twig", "C:\\xampp\\htdocs\\ipd15-david\\slimtest\\templates\\index.html.twig");
    }
}
