<?php

session_start();
require_once 'vendor/autoload.php';

// ------------------------------ Monolog ------------------------------ 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


// ------------------------------ MeekroDB ------------------------------ 
DB::debugMode();

DB::$host = 'localhost';
DB::$user = 'slimtest';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'slimtest';
DB::$port = 3306;  // 3306 for home, 3333 for school
DB::$encoding = 'utf8';

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("Query error: " . $params['query']);
    echo "Error: " . $params['error'] . "<br>\n";
    echo "Query: " . $params['query'] . "<br>\n";
    http_response_code(500);
    $app->render("fatal_error.html.twig", array(
        'errorList' => array(
            "Error: " => $params['error'],
            "Query: " => $params['query']
        )
    ));


    die; // don't want to keep going if a query broke
}

// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');
// ------------------- Slim creation and setup ------------------- 
// -------------------------------- / -------------------------------- 
$app->get('/', function() use ($app) {
    $resultist = DB::query("SELECT * FROM products");
    $app->render('index.html.twig', array('todosList' => $resultist));
});

// ------------------------------ /product/id ------------------------------ 
$app->get('/product/:id', function($id) use ($app) {
    $result = DB::queryFirstRow("SELECT * FROM products WHERE id = %s", $id);
    if ($result) {
        $app->notFound();
    }
    var_dump($result);
    $app->render("product_view.html.twig", array('p' => $result));
});

// -------------------------------- /passport/add -------------------------------- 
$app->get('/passport/add', function() use ($app) {
    // state 1: first show
    $app->render('passport_add.html.twig');
});
$app->post('/passport/add', function() use ($app) {

    // receiving submission
    var_dump($_POST);  // for debugging
    echo '<hr />';

    $number = $app->request()->post('number');
    $name = $app->request()->post('name');
    $valueList = array('number' => $number, 'name' => $name);


    // verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name($name) must be between 2-100 characters long");
    }
    

//    if ($_FILES['image']) {
    $image = $_FILES['image'];
    $imageInfo = getimagesize($image['tmp_name']);
    if (!$imageInfo) {
        array_push($errorList, "File does not look like a valid image");
    } else {
        // Never allow '..' in the file name
        if (strstr($image['name'], '..')) {
            array_push($errorList, "File name invalid");
        }
        // only allow select extensions
        $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
        IF (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
            array_push($errorList, "File extension invalid");
        }

        // check mime-type submitted
//        $mineType = $image['type']; // TODO: USE GETIMAGESIZE RESULT MIME-TYPE INSTEAD
        $mineType = $imageInfo['mime'];
        if (!in_array($mineType, array("image/gif", "image/jpeg", "image/jpg", "image/png"))) {
            array_push($errorList, "File type invalid.");
        }

    }
//    } else {
//        array_push($errorList, "Image file has not been selected.");
//    }


    if (!$errorList) {
        // state 2: successful submission
//        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        $imageData = file_get_contents($image['tmp_name']);
        DB::insert('products', array(
            'number' => $number,
            'name' => $name,
            'image' => $imageData,
            'mimeType' => $mineType
        ));
        echo 'success';
    } else {
        // state 3: failed submission
        $app->render('product_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
        ));
    }
});



// -------------------------------- /products/add -------------------------------- 
$app->get('/products/add', function() use ($app) {
    // state 1: first show
    $app->render('product_add.html.twig');
});
$app->post('/products/add', function() use ($app) {

    // receiving submission
    var_dump($_POST);  // for debugging
    echo '<hr />';
    $name = $app->request()->post('name');
    $description = $app->request()->post('description');
    $price = $app->request()->post('price');
    $valueList = array('name' => $name, 'description' => $description, 'price' => $price);


    // verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name($name) must be between 2-100 characters long");
    }
    if (strlen($description) < 2 || strlen($description) > 2000) {
        array_push($errorList, "Description($description) must be between 2-2000 characters long");
    }
    if (!is_numeric($price) || $price < 0 || $price > 999999.99) {
        array_push($errorList, "Price must be 0-999999.99");
    }
    if (isset($_FILES['image'])) {
        $image = $_FILES['image'];
        $imageInfo = getimagesize($image['tmp_name']);
        if (!$imageInfo) {
            array_push($errorList, "File does not look like a valid image");
        } else {
            // Never allow '..' in the file name
            if (strstr($image['name'], '..')) {
                array_push($errorList, "File name invalid");
            }
            // only allow select extensions
            $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
            IF (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
                array_push($errorList, "File extension invalid");
            }

            // do not allow to override an existing file
            if (file_exists('uploads/' . $image['name'])) {
                array_push($errorList, "File name already exists refusing to override.");
            }
        }
    }


    if (!$errorList) {
        // state 2: successful submission
        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        DB::insert('products', array(
            'name' => $name,
            'description' => $description,
            'price' => $price,
            'imagePath' => $image['name']
        ));
        echo 'success';
    } else {
        // state 3: failed submission
        $app->render('product_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
        ));
    }
});

// ------------------------------ Map --------------------------------
// Just an example - one handler for both get and post
$app->map('/multimethod', function() use ($app) {
    if ($app->request()->getMethod() == 'GET') {
        
    }
})->via('GET', 'POST');


// ------------------------------ /:action/:id --------------------------------
$app->get('/:action(/:id)', function($action, $id = -1) use ($app, $log) {
    if (($action == 'add' && $id != -1) || ($action == 'edit' && $id == -1)) {
        $app->notFound();
        return;
    }
    //
    if ($action == 'edit') {
        $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);

        if (!$todo) {
            $app->notFound();
            return;
        }
        $log->debug("preparing to edit todo with id=" . $id);
        $app->render('addedit.html.twig', array(
            'action' => 'edit',
            'v' => $todo)
        );
    } else {
        $app->render('addedit.html.twig', array('action' => 'add'));
    }
})->conditions(array('action' => '(add|edit)', 'id' => '[0-9]+'));

$app->post('/:action(/:id)', function($action, $id = -1) use ($app, $log) {
    if (($action == 'add' && $id != -1) || ($action == 'edit' && $id == -1)) {
        $app->notFound();
        return;
    }
    //
//    var_dump($_POST);  // for debugging
    $id = $app->request()->post('id');
    $task = $app->request()->post('task');
    $dueDate = $app->request()->post('dueDate');
    $isDone = $app->request()->post('isDone', 'false') == 'true';
    $todo = array('id' => $id, 'task' => $task, 'dueDate' => $dueDate, 'isDone' => $isDone ? "true" : "false");

    //
    $errorList = array();
    if (strlen($task) < 1 || strlen($task) > 100) {
        array_push($errorList, "Task must be 1-100 characters long");
    }
    if (date("Y-m-d", strtotime($dueDate)) != $dueDate) {
        array_push($errorList, "Date format is invalid");
    }
    //
    if (!$errorList) {
        if ($action == 'add') {
            // insert
            DB::insert('todos', $todo);
            $currentTodoId = DB::insertID();
            $log->debug("Adding todo with new id=" . $currentTodoId);
        } else {
            // update
            DB::update('todos', $todo, "id=%i", $todo['id']);
            $currentTodoId = $todo['id'];
            $log->debug("Updating todo with new id=" . $currentTodoId);
        }
        $app->render('todo_add_success.html.twig', array('currentTodoId' => $currentTodoId));
    } else {
        // state 3: failed submission
        $app->render('addedit.html.twig', array(
            'v' => $todo,
            'errorList' => $errorList
        ));
    }
})->conditions(array('action' => '(add|edit)', 'id' => '[0-9]+'));


// ------------------------------ For Debugging ------------------------------ 
$app->get('/session', function() {
    echo "<pre>SESSION:\n\n";
    // var_dump($_SESSION);
    print_r($_SESSION);
});

// ------------------------------ Run ------------------------------ 
$app->run();
