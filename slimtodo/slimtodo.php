<?php

session_start();
require_once 'vendor/autoload.php';

// ------------------------------ Monolog ------------------------------ 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


// ------------------------------ MeekroDB ------------------------------ 
DB::debugMode();

DB::$host = 'localhost';
DB::$user = 'slimtodo';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'slimtodo';
DB::$port = 3333;  // 3306 for home, 3333 for school
DB::$encoding = 'utf8';

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("Query error: " . $params['query']);
    echo "Error: " . $params['error'] . "<br>\n";
    echo "Query: " . $params['query'] . "<br>\n";
    http_response_code(500);
    $app->render("fatal_error.html.twig", array(
        'errorList' => array(
            "Error: " => $params['error'],
            "Query: " => $params['query']
        )
    ));


    die; // don't want to keep going if a query broke
}

// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');
// ------------------- Slim creation and setup ------------------- 
// -------------------------------- / -------------------------------- 
$app->get('/', function() use ($app) {
    $todosList = DB::query("SELECT id, task, dueDate, isDone FROM todos ORDER BY id");
    $app->render('index.html.twig', array('todosList' => $todosList));
});

// ------------------------------ /todo/id ------------------------------ 
$app->get('/todo/:id', function($id) use ($app) {
    $todo = DB::queryFirstRow("SELECT id, task, dueDate, isDone FROM todos  WHERE id = %s", $id);
    $app->render("addedit.html.twig", array('v' => $todo));
});


// -------------------------------- /add-w -------------------------------- 
$app->get('/add-w', function() use ($app) {
    // state 1: first show
    $app->render('todo_add.html.twig');
});
$app->post('/add-w', function() use ($app) {
//    if (!isset($_SESSION['user'])) {
//        $app->render('access_denied.html.twig');
//        return;
//    }
//    
    // receiving submission
    var_dump($_POST);  // for debugging
    $id = $app->request()->post('id');
    $task = $app->request()->post('task');
    $dueDateTime = strtotime($app->request()->post('dueDate'));
    $dueDate = date("Y-m-d", $dueDateTime);
    if (empty($app->request()->post('isDone'))) {
        $isDone = "true";
    } else {
        $isDone = "false";
    }
    $currentTodo = array('id' => $id, 'task' => $task, 'dueDate' => $dueDate, 'isDone' => $isDone);

    // verify submission
    $errorList = array();
    if (strlen($task) < 2 || strlen($task) > 100) {
        array_push($errorList, "Task must be between 2-100 characters long");
    }
    $today = strtotime("today");

    if ($dueDateTime < $today) {
        array_push($errorList, "DueDate must after now.");
    }

    if (!$errorList) {
        // state 2: successful submission
        if ($id !== "") {
            DB::update('todos', array(
                'id' => $id,
                'task' => $task,
                'dueDate' => $dueDate
            ));
            $currentTodoId = $currentTodo['id'];
        } else {
            DB::insert('todos', array(
                'id' => NULL,
                'task' => $task,
                'dueDate' => $dueDate,
                'isDone' => $isDone
            ));
            $currentTodoId = DB::insertId();
        }
        $app->render('todo_add_success.html.twig', array('currentTodoId' => $currentTodoId));
    } else {
        // state 3: failed submission
        $app->render('todo_add.html.twig', array(
            'v' => $currentTodo,
            'errorList' => $errorList
        ));
    }
});



// ------------------------------ Map --------------------------------
// Just an example - one handler for both get and post
$app->map('/multimethod', function() use ($app) {
    if ($app->request()->getMethod() == 'GET') {
        
    }
})->via('GET', 'POST');


// ------------------------------ /:action/:id --------------------------------
$app->get('/:action(/:id)', function($action, $id = -1) use ($app, $log) {
    if (($action == 'add' && $id != -1) || ($action == 'edit' && $id == -1)) {
        $app->notFound();
        return;
    }
    //
    if ($action == 'edit') {
        $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);

        if (!$todo) {
            $app->notFound();
            return;
        }
        $log->debug("preparing to edit todo with id=" . $id);
        $app->render('addedit.html.twig', array(
            'action' => 'edit',
            'v' => $todo)
        );
    } else {
        $app->render('addedit.html.twig', array('action' => 'add'));
    }
})->conditions(array('action' => '(add|edit)', 'id' => '[0-9]+'));

$app->post('/:action(/:id)', function($action, $id = -1) use ($app, $log) {
    if (($action == 'add' && $id != -1) || ($action == 'edit' && $id == -1)) {
        $app->notFound();
        return;
    }
    //
//    var_dump($_POST);  // for debugging
    $id = $app->request()->post('id');
    $task = $app->request()->post('task');
    $dueDate = $app->request()->post('dueDate');
    $isDone = $app->request()->post('isDone', 'false') == 'true';
    $todo = array('id' => $id, 'task' => $task, 'dueDate' => $dueDate, 'isDone' => $isDone ? "true" : "false");

    //
    $errorList = array();
    if (strlen($task) < 1 || strlen($task) > 100) {
        array_push($errorList, "Task must be 1-100 characters long");
    }
    if (date("Y-m-d", strtotime($dueDate)) != $dueDate) {
        array_push($errorList, "Date format is invalid");
    }
    //
    if (!$errorList) {
        if ($action == 'add') {
            // insert
            DB::insert('todos', $todo);
            $currentTodoId = DB::insertID();
            $log->debug("Adding todo with new id=" . $currentTodoId);
        } else {
            // update
            DB::update('todos', $todo, "id=%i", $todo['id']);
            $currentTodoId = $todo['id'];
            $log->debug("Updating todo with new id=" . $currentTodoId);
        }
        $app->render('todo_add_success.html.twig', array('currentTodoId' => $currentTodoId));
    } else {
        // state 3: failed submission
        $app->render('addedit.html.twig', array(
            'v' => $todo,
            'errorList' => $errorList
        ));
    }
})->conditions(array('action' => '(add|edit)', 'id' => '[0-9]+'));


// ------------------------------ For Debugging ------------------------------ 
$app->get('/session', function() {
    echo "<pre>SESSION:\n\n";
    // var_dump($_SESSION);
    print_r($_SESSION);
});

// ------------------------------ Run ------------------------------ 
$app->run();
