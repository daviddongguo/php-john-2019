<?php

session_start();
require_once 'vendor/autoload.php';

// ------------------------------ Monolog ------------------------------ 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


// ------------------------------ MeekroDB ------------------------------ 
//DB::debugMode();

DB::$host = 'localhost';
DB::$user = 'todorest';
DB::$password = 'vuxunjqTbm5S7sAq';
DB::$dbName = 'todorest';
DB::$port = 3333;
DB::$encoding = 'utf8';

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode("500 - internal error");
    die; // don't want to keep going if a query broke
}

// ------------------- Slim creation and setup ------------------- 
$app = new \Slim\Slim();
// not use twig
//$app->response()->header('content-type', 'application/json');
$app->response()->header('content-type', 'application/json');
\Slim\Route::setDefaultConditions(array(
    'id' => '\d+'
));
// ------------------- Slim creation and setup ------------------- 
// ----------------------------- get / ---------------------- 
$app->get('/', function() use($app, $log) {
    $todoList = DB::query("SELECT * FROM todos");
//    print_r($todoList);
    echo '<hr />';
    echo json_encode($todoList, JSON_PRETTY_PRINT);
    echo '<hr />';
});


// ----------------------------- get /todos ---------------------- 
$app->get('/todos', function() use($app, $log) {
    $todoList = DB::query("SELECT * FROM todos");
    echo json_encode($todoList, JSON_PRETTY_PRINT);
});

// ----------------------------- get /todos/id ---------------------- 
$app->get('/todos/:id', function($id) use($app, $log) {
    $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);
    echo json_encode($todo, JSON_PRETTY_PRINT);
});

// ----------------------------- post /todos ---------------------- 
$app->post('/todos', function() use($app, $log) {
    $json = $app->request()->getBody();
    $todo = json_decode($json, true); // true to force it to return associative array
    // FIXME: validate $todo
    if (isTodoValid($todo) !== TRUE) {
        $app->response()->status(400);
        $log->err("POST /todos failed: " . isTodoValid($todo));
        echo json_encode(isTodoValid($todo));
        return;
    }
    DB::insert('todos', $todo);
    $app->response()->status(201);
    echo json_encode(DB::insertId());
});

// ----------------------------- put /todos/id ---------------------- 
$app->put('/todos/:id', function($id) use($app, $log) {
    $json = $app->request()->getBody();
    $todo = json_decode($json, true); // true to force it to return associative array
    // FIXME: validate $todo


    $exiting = DB::queryOneRow("SELECT * FROM todos WHERE id=%i", $id);
    if (!$exiting) {
        $app->response()->status(404);
        echo json_encode(false);
    } else {
        DB::update('todos', $todo, 'id=%i', $id);
        $log->debug(sprintf("PUT /todo/%s succeed", $id));
        echo json_encode(true);
    }
});


// ----------------------------- delete /todos/id ---------------------- 
$app->delete('/todos/:id', function($id) use($app, $log) {
    DB::delete('todos', 'id=%i', $id);
    echo json_encode(DB::affectedRows() != 0);
});

// returns TRUE if todo is valid, otherwise a string describing the problem
function isTodoValid($todo) {
    if (is_null($todo)) {
        return "JSON parsing failed.";
    }
    if (count($todo) != 3 && count($todo) != 4) {
        return "Invalid number of values";
    }
    if (!isset($todo['task']) ||
            !isset($todo['dueDate']) ||
            !isset($todo['isDone'])) {
        return "Required field missing.";
    }
    if (strlen($todo['task']) < 1 || strlen($todo['task']) > 100) {
        return "Task must be 1-100 characters long.";
    }
    if (date("Y-m-d", strtotime($todo['dueDate'])) != $todo['dueDate']) {
        return "Date format is invalid";
    }
    if ($todo['isDone'] != 0 && $todo['isDone'] != 1) {
        return "isDone must be 0 or 1.";
    }


    return TRUE;
}

// ------------------------------ Run ------------------------------ 
$app->run();
